# Space-time machine

Collection of projects by Mark van der Net.
Some of my thoughts about the infrastructure and architecture.

## Architecture

### Raw data ingestion
*Here I have specific ideas about*

```mermaid
graph LR;
    A(fa:fa-calendar Scheduler)-->|calls|B(fa:fa-filter Data Collector)
    B-->|1. request|C[fa:fa-globe External Source]
    C-->|2. return|B
    B-->|3. persists data on|D[fa:fa-database Ingestion Database];
```

Scheduler:

  - Airflow/Prefect
  - Contains schedule for ingestion of data, in single instance (with backup)
  - Calls Data Collector APIs
  - Keeps schedule, knows when calls are missed and can recall for retry

Data Collector:

  - Custom code, e.g. scrapper
  - Exposes API: FastAPI + Pydantic model
  - Scheduler calls API with specific parameters, e.g. get data from-to date(time)
  - Scalable; with single codebase, data collector can be run on multiple nodes

Ingestion Database:

  - Postgres/CockroachDB
  - Stores incoming data "raw", no transformations
  - To be transformed in the next step: data transformation/interpretation
  - One or more tables per Data Collector, tables initiated by Data Collector
  - Data can be persisted here for (future) (re)interpretation, purged after transformation of purged after some set time

Challenges:

  - How to version a single API codebase for Data Collectors so it can be implemented and updated for multiple instances?
  - Single codebase with growing number of scrapper etc. Through exposed API specific Data Collector can be called 
  - Something with Helm charts, other infrastructure-by-code solution?


### Data transformation/interpretation
*Here my ideas are more vague*

```mermaid
graph LR;
    A[fa:fa-database Ingestion Database]-->|data flow|B(ETL worker)
    B-->|data flow|C[fa:fa-database Space-Time Database]
```

ETL worker:

  - Extract data from ingestion database and loads it transformed into Space-Time Database

Space-Time Database:

  - Postgres/PostGIS/Timescale
  - Stores data in a way, so it can be combined with each other and can be is relevant for the frontend

More tools:

  - Apache Spark, for distributed (heavy) transformations?
  - Scheduler in the form of Airflow or Airflow/Prefect
