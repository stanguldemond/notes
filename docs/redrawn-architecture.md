# Redrawn architecture

- This is an experiment with Mermaid Flowcharts ([documentation](https://mermaid-js.github.io/mermaid/#/flowchart?id=flowcharts-basic-syntax)).
- Some nodes are hoverable and clickable

```mermaid
graph TD
    A((Data sources))-->B[fa:fa-filter Data collector]
    B-->D[fa:fa-bars Data catalog]

    B-->C[fa:fa-cogs Geo-time engine]
    C-->E[fa:fa-database Data warehouse]
    E-->F[fa:fa-calculator Analytics engine]
    F-->F1[Search/Analytics jobs]
    F1-->G[fa:fa-plug API]
    G-->H[fa:fa-calendar Task manager]
    H-->B
    click E "https://www.cockroachlabs.com/product/" "CockroachDB"
    click F "https://spark.apache.org/" "Apache Spark"
    click G "https://fastapi.tiangolo.com/" "FastAPI"
    click H "https://www.prefect.io/" "Prefect.io"
```