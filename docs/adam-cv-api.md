# Amsterdam Computer Vision API (ACV-API)

## From image to objects

When some service would like to know what objects are an image they can talk to the Computer Vision API.
They can request either just the detection result, but optionally the same picture with privacy blurring.

```mermaid
graph TD
    A((ODK/SIA/VTH))-->|produces|B[/fa:fa-image Image + meta data/]
    B-->|send to|C[fa:fa-eye Computer Vision API]
    C-->|run image through|D[fa:fa-search Privacy model]
    C-->|run image through|E[fa:fa-search Garbage model]
    C-->|run image through|F[fa:fa-search Container model]
    D-->|blurred image|G[fa:fa-file File storage]
    E-->|results|H[fa:fa-database Detection result database]
    F-->|results|H
    H-->|detected objects|I[fa:fa-paper-plane CV API response]
    G-.->|"(optional) blurred image"|I
    style C fill:#f9f,stroke:#333,stroke-width:3px
    style I fill:#f9f,stroke:#333
```

- Computer Vision API: FastAPI
- \* model: Pre-trained YOLOv5 model
- File storage: Azure Data Lake Storage
- Detection result database: PostgreSQL

## Persisting images with annotations

Next to a model that works great, the real valuable data are the annotations.
When you have the original image plus annotations of different objects on that image, you can train and retrain a model to optimize its performance.
In some cases you might want to focus on speed, in others on accuracy, are some combination, depending on the use case.
Also when a new object detection technology is created, these annotations can be reused.

```mermaid
classDiagram
    Image "1" --> "*" Annotation
    Image "1" --> "*" DetectionResult
    ImageFile "1" --> "1" Image
    Annotation "1" --> "1" Object
    DetectionResult "1" --> "1" Object
    class ImageFile {
        -string file_name
        -string file_location
    }
    class Image {
        -UUID id
        -string file_name
        -string file_location
        -string project_name
        -is_annotated() boolean
        -has_results() boolean
    }
    class Annotation {
        -UUID id
        -UUID image_id
        -UUID object_id
        -JSON bounding_boxes
    }
    class DetectionResult {
        -UUID id
        -UUID image_id
        -UUID object_id
        -float confidence
        -JSON bounding_box
        -JSON meta_data
        -boolean is_correct
    }
    class Object {
        -UUID id
        -string title
        -boolean is_privacy
    }
```

- ImageFile: stored in file storage
- Image: stored in relational database
- Object: idem
- Annotation: idem
- DetectionResult: idem, can later be marked as incorrect for retraining a model
